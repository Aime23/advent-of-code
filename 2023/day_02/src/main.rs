use std::{
    fmt::format,
    fs::File,
    io::{BufRead, BufReader},
    ops::Index,
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut values: Vec<Game> = Vec::new();

    for (_, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let value = parse_line(line.as_str());
        values.push(value);
    }

    println!("{}", values.len());

    let out = values.iter()
    // .filter(| game | game.red <= 12 && game.green <= 13 && game.blue <= 14 )
    .fold(0, |a, b| a + b.red * b.green * b.blue);

    println!("{}", out)
}

struct Game {
    id: i32,
    red: i32,
    green: i32,
    blue: i32,
}

fn parse_line(line: &str) -> Game {
    let id = line.split(" ").nth(1).unwrap().replace(":", "").parse::<i32>().unwrap();
    let mut red = 0;
    let mut green = 0;
    let mut blue = 0;
    line.split(":")
        .nth(1)
        .unwrap()
        .replace(";", ",")
        .split(",")
        .for_each(|item| {
            let parts = item.split(" ").collect::<Vec<&str>>();
            let count = parts[1].parse::<i32>().unwrap();
            let color = parts[2];
            match color {
                "red" => red = std::cmp::max(red, count),
                "green" => green = std::cmp::max(green, count),
                "blue" => blue = std::cmp::max(blue, count),
                _ => ()
            }
        });
    Game {
        id,
        red,
        green,
        blue,
    }
}
