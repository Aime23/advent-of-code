use std::{
    fs::File,
    io::{BufRead, BufReader}, cell::{Cell, RefCell},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let cards: Vec<Card> = reader
        .lines()
        .map(|line| parse_line(line.unwrap().as_str()))
        .collect();

    cards.iter().for_each(|card| card.solve(&cards));

    let total: u64 = cards.iter().map(|card| card.recursive_count_wining()).sum();

    println!("{}", total);
}

struct Card<'a> {
    id: u64,
    winning_numbers: Vec<u64>,
    numbers: Vec<u64>,
    winning_count: Cell<Option<u64>>,
    won_cards: RefCell<Vec<&'a Card<'a>>>
}

impl<'a> Card<'a> {
    fn solve(&self, cards: &'a Vec<Card<'a>>) {
        if self.winning_count.get().is_none() {
            self.winning_count.set(Some(
                self.numbers
                    .iter()
                    .filter(|number| self.winning_numbers.contains(&number))
                    .count() as u64,
            ));
        }

        let count = self.winning_count.get().unwrap();
        let mut won_cards = self.won_cards.borrow_mut();
        cards[(self.id as usize)..(self.id as usize + count as usize)].iter().for_each(| won_card| won_cards.push(won_card));
    }

    fn recursive_count_wining(&self) -> u64 {
        self.won_cards.borrow().iter().map(|card| card.recursive_count_wining()).sum::<u64>() + 1
    }
}

fn parse_line<'a>(line: &str) -> Card<'a> {
    let [identification, all_numbers] = line.split(':').collect::<Vec<&str>>()[0..2] else {
        todo!()
    };
    let id: u64 = identification
        .split_whitespace()
        .nth(1)
        .unwrap()
        .parse()
        .unwrap();

    let [winning_numbers, numbers] = all_numbers.split('|').collect::<Vec<&str>>()[0..2] else {
        todo!()
    };
    let winning_numbers: Vec<u64> = winning_numbers
        .split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect();
    let numbers: Vec<u64> = numbers
        .split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect();

    Card {
        id,
        winning_numbers,
        numbers,
        winning_count: Cell::from(None),
        won_cards: RefCell::from(Vec::new())
    }
}
