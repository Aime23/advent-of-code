use std::{
    fmt::format,
    fs::File,
    io::{BufRead, BufReader},
    ops::{Index, Range},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut part_numbers: Vec<PartNumber> = vec![];
    let mut symbols: Vec<Symbol> = vec![];


    for (index, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let mut value = parse_line(line.as_str(), index as u64);
        part_numbers.append(&mut value.0);
        symbols.append(&mut value.1);
    }

    let gear_symbols: Vec<&Symbol> = symbols.iter().filter(|symbol| symbol.symbol.eq(&'*')).collect();

    let total = gear_symbols.iter().fold(0, |sum, symbol| {

        let adjacent_part_numbers: Vec<&PartNumber> = part_numbers.iter()
        .filter(|part| ((part.x.start.saturating_sub(1))..(part.x.end + 1)).contains(&symbol.x) && ((part.y.saturating_sub(1))..(part.y + 2)).contains(&symbol.y)).collect();
        if adjacent_part_numbers.len() == 2 {
            return sum + (adjacent_part_numbers[0].value * adjacent_part_numbers[1].value);
        } else {
            return sum;
        }
    });

    // let valid_part_numbers = part_numbers.iter().filter(|part|{
    //     let x_range = (part.x.start.saturating_sub(1))..(part.x.end + 1);
    //     let y_range = (part.y.saturating_sub(1))..(part.y + 2);

    //     let found_symbol = gear_symbols.filter(|symbol| x_range.contains(&symbol.x) && y_range.contains(&symbol.y));
    //     return found_symbol.is_some();
    // });

    // let total = valid_part_numbers.fold(0, |a, b| a + b.value);


    println!("{:?}", total)
}

#[derive(Debug)]
struct Symbol {
    x: u64,
    y: u64,
    symbol: char
}

#[derive(Debug)]
struct PartNumber {
    x: Range<u64>,
    y: u64,
    value: u64,
}

fn parse_line(line: &str, y: u64) -> (Vec<PartNumber>, Vec<Symbol>) {
    let chars = line.chars().collect::<Vec<char>>();
    let mut i = 0;
    let mut numbers: Vec<PartNumber> = vec![];
    let mut parts: Vec<Symbol> = vec![];
    while i < line.len() {
        if chars[i].is_digit(10) {
            let mut offset = 0;
            while i+offset < chars.len() && chars[i + offset].is_digit(10) {
                offset += 1
            }
            let value: u64 = line[i..i + offset].parse().unwrap();
            numbers.push(PartNumber {
                x: (i as u64)..(i as u64 + offset as u64),
                y,
                value,
            });
            i += offset -1;
        } else if !chars[i].eq(&'.') {
            parts.push(Symbol { x: i as u64, y, symbol: chars[i] })
        }
        i += 1;
    }
    return (numbers, parts);
}
