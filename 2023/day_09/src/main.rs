use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let readings: Vec<Vec<i64>> = reader.lines().map(|s| parse_line(s.unwrap())).collect();
    let nexts: Vec<i64> = readings
        .into_iter()
        .map(|reading| find_next(reading))
        .collect();

    let out: i64 = nexts.iter().sum();

    println!("{}", out)
}

fn parse_line(line: String) -> Vec<i64> {
    line.split_whitespace()
        .map(|s| s.parse().unwrap())
        .collect()
}

fn find_next(reading: Vec<i64>) -> i64 {
    let mut sub_sequences: Vec<Vec<i64>> = Vec::new();
    sub_sequences.push(reading);
    while !every_zero(sub_sequences.last().unwrap()) {
        sub_sequences.push(build_sub_sequence(sub_sequences.last().unwrap()))
    }

    (1..(sub_sequences.len()))
        .rev()
        .fold(0, |acc, i| sub_sequences[i - 1].first().unwrap() - acc)
}

fn build_sub_sequence(array: &Vec<i64>) -> Vec<i64> {
    (0..(array.len() - 1))
        .map(|i| array[i + 1] - array[i])
        .collect()
}

fn every_zero(array: &Vec<i64>) -> bool {
    array[0] == 0 && array.iter().all(|x| x == &0)
}
