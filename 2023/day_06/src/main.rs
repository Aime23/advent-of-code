use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut values: Vec<i32> = Vec::new();

    let mut lines = reader.lines();

    let time = parse_line(lines.next().unwrap().unwrap());
    let distance = parse_line(lines.next().unwrap().unwrap());

    let mut out = 0;
    for t in 0..time {
        let moving_time = time - t;
        let d = moving_time * t;
        if d > distance {
            out += 1
        }
    }
    println!("{}", out);
}

fn parse_line(line: String) -> u64 {
    line.split(":")
        .nth(1)
        .unwrap()
        .replace(" ", "")
        .parse::<u64>()
        .unwrap()
}
