use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};


fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut lines = reader.lines();

    let directions = parse_sequence(lines.next().unwrap().unwrap());

    lines.next();

    let mut nodes: Vec<Node> = lines.map(|s| Node::from(s.unwrap())).collect();

    let current_nodes: Vec<&Node> = nodes
        .iter()
        .filter_map(|node| {
            if node.name.ends_with('A') {
                Some(node)
            } else {
                None
            }
        })
        .collect();

    let mut map: HashMap<String, &Node> = HashMap::new();

    nodes.iter().for_each(|node| {
        map.insert(node.name.clone(), &node);
    });

    let number_of_steps: Vec<u64> = current_nodes
        .iter()
        .map(|node_ref| {
            let mut node = node_ref;
            let mut count = 0;
            loop {
                let direction = &directions[count % directions.len()];

                node = {
                    match direction {
                        Direction::LEFT => map.get(&node.left).unwrap(),
                        Direction::RIGHT => map.get(&node.right).unwrap(),
                    }
                };

                if node.name.ends_with('Z') {
                    break;
                }
                count += 1;
            };
            return count as u64 + 1;
        })
        .collect();

    let min_step: u64 = number_of_steps.into_iter().reduce(|acc, x| lcm(acc, x)).unwrap();
    println!("{}", min_step)

}

fn gcd(a: u64, b: u64) -> u64 {
    if b == 0 {
        return a;
    }
    return gcd(b, a % b)
}

fn lcm(a: u64, b: u64) -> u64 {
    return a * b / gcd(a, b);
}

enum Direction {
    LEFT,
    RIGHT,
}

struct Node {
    name: String,
    left: String,
    right: String,
}

fn parse_sequence(line: String) -> Vec<Direction> {
    line.chars()
        .map(|c| match c {
            'L' => Direction::LEFT,
            'R' => Direction::RIGHT,
            _ => Direction::LEFT,
        })
        .collect()
}

impl From<String> for Node {
    fn from(value: String) -> Self {
        let name = &value[0..3];
        let left = &value[7..10];
        let right = &value[12..15];
        Self {
            name: name.to_owned(),
            left: left.to_owned(),
            right: right.to_owned(),
        }
    }
}
