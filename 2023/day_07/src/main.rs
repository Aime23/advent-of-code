use std::{
    cmp::Ordering,
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut hands: Vec<Hand> = reader.lines().map(|s| Hand::from(s.unwrap())).collect();

    hands.sort_by(|a, b| a.cmp(b));

    let out = hands
        .iter()
        .enumerate()
        .map(|(index, hand)| (index as u64 + 1) * hand.bid)
        .fold(0, |acc, hand| acc + hand);

    println!("{}", out);
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Card(u32);

impl From<char> for Card {
    fn from(value: char) -> Self {
        if value.is_numeric() {
            let value = value.to_digit(10).unwrap();
            return Self(value);
        }

        let value = match value {
            'T' => 10,
            'J' => 1,
            'Q' => 12,
            'K' => 13,
            'A' => 14,
            _ => 0, //Does not handle wrong values
        };

        return Self(value);
    }
}
struct Hand {
    bid: u64,
    cards: [Card; 5],
}

impl From<String> for Hand {
    fn from(value: String) -> Self {
        let values = value.split_whitespace().collect::<Vec<&str>>();
        let cards = values[0];
        let bid = values[1];
        let bid = bid.parse::<u64>().unwrap();

        let cards = cards.chars().map(|c| Card::from(c)).collect::<Vec<Card>>();
        return Self {
            bid,
            cards: cards.try_into().unwrap(),
        };
    }
}

impl Hand {
    fn get_type(&self) -> u32 {
        let mut map: HashMap<u32, u32> = HashMap::new();
        let mut jokers = 0;

        self.cards.iter().for_each(|card| {
            if card.0 != 1 {
                if !map.contains_key(&card.0) {
                    map.insert(card.0, 0);
                }
                map.insert(card.0, map.get(&card.0).unwrap() + 1);
            } else {
                jokers += 1;
            }
        });

        let non_jokers = 5 - jokers;

        let card_types = map.keys().len();

        let mut values = map.values().collect::<Vec<&u32>>();
        values.sort();
        if card_types == 0 || card_types == 1 {
            return 7;
        } else if card_types == 2 {
            if values[0].eq(&1) {
                return 6;
            } else {
                return 5;
            }
        } else if card_types == 3 {
            if non_jokers == 5 {
                if values.iter().any(|&x| x.eq(&3)) {
                    return 4;
                } else {
                    return 3;
                }
            } else if non_jokers == 4 {
                return 4;
            } else {
                return 4;
            }
        } else {
            match card_types {
                4 => return 2,
                5 => return 1,
                _ => 0,
            }
        }
    }
}

impl Eq for Hand {}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        self.bid == other.bid && self.cards == other.cards
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.cards.partial_cmp(&other.cards)
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.get_type().cmp(&other.get_type()) {
            Ordering::Equal => self.cards.cmp(&other.cards),
            value => value,
        }
    }
}
