use std::{
    fs::File,
    io::{BufRead, BufReader},
    ops::Range,
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut lines = reader.lines();

    let seeds = parse_seed(lines.next().unwrap().unwrap().as_str());
    let mut maps = Vec::new();
    let max = seeds.iter().max_by(|a, b| a.end.cmp(&b.end)).unwrap().end;

    lines.next(); //Discard second line

    let mut lines_of_map: Vec<String> = Vec::new();

    for (index, line) in lines.enumerate() {
        let line = line.unwrap();

        if line.is_empty() {
            maps.push(parse_map(lines_of_map, max));
            lines_of_map = Vec::new();
        } else {
            lines_of_map.push(line);
        }
    }

    maps.push(parse_map(lines_of_map, max));

    let out: u64 = find_matching(
        &maps,
        seeds,
        &String::from("seed"),
        String::from("location"),
    )
    .iter()
    .min_by(|a, b| a.start.cmp(&b.start))
    .unwrap()
    .start;

    println!("{}", out)
}

#[derive(Debug)]
struct Map {
    source: String,
    target: String,
    map: Vec<(Range<u64>, Range<u64>)>,
}

fn parse_map(lines: Vec<String>, max: u64) -> Map {
    let header = parse_header(lines[0].as_str());

    let mut entries: Vec<(Range<u64>, Range<u64>)> = lines[1..]
        .iter()
        .map(|line| parse_map_entry(line))
        .collect();

    {
        let mut source_ranges: Vec<&Range<u64>> = Vec::new();
        let mut target_ranges: Vec<&Range<u64>> = Vec::new();

        for i in 0..entries.len() {
            source_ranges.push(&entries[i].0);
            target_ranges.push(&entries[i].1);
        }

        source_ranges.sort_by(|a, b| a.start.cmp(&b.start));
        target_ranges.sort_by(|a, b| a.start.cmp(&b.start));

        let mut missing_source_ranges: Vec<Range<u64>> = Vec::new();
        let mut missing_target_ranges: Vec<Range<u64>> = Vec::new();

        for i in 0..source_ranges.len() {
            if i == source_ranges.len() - 1 {
                let range = source_ranges[i];
                if range.end < max {
                    missing_source_ranges.push(range.end..max);
                }
            } else {
                if source_ranges[i + 1].start - source_ranges[i].end != 0 {
                    missing_source_ranges.push(source_ranges[i].end..source_ranges[i + 1].start);
                }
            }
        }

        for i in 0..target_ranges.len() {
            if i == target_ranges.len() - 1 {
                let range = target_ranges[i];
                if range.end < max {
                    missing_target_ranges.push(range.end..max);
                }
            } else {
                if target_ranges[i + 1].start - target_ranges[i].end != 0 {
                    missing_target_ranges.push(target_ranges[i].end..target_ranges[i + 1].start);
                }
            }
        }

        missing_source_ranges.sort_by(|a, b| a.start.cmp(&b.start));
        missing_target_ranges.sort_by(|a, b| a.start.cmp(&b.start));

        let mut source_index = 0;
        let mut target_index = 0;

        loop {
            if source_index >= missing_source_ranges.len()
                || target_index >= missing_target_ranges.len()
            {
                break;
            }

            let source_range = &mut missing_source_ranges[source_index];
            let target_range = &mut missing_target_ranges[target_index];

            let source_count = source_range.end - source_range.start;
            let target_count = target_range.end - target_range.start;

            if source_count == target_count {
                entries.push((source_range.clone(), target_range.clone()));

                source_index += 1;
                target_index += 1;
            } else if source_count < target_count {
                entries.push((
                    source_range.clone(),
                    target_range.start..target_range.start + source_count as u64,
                ));
                target_range.start += source_count as u64;
                source_index += 1;
            } else {
                entries.push((
                    source_range.start..source_range.start + target_count as u64,
                    target_range.clone(),
                ));
                source_range.start += target_count as u64;
                target_index += 1;
            }
        }
    }

    Map {
        source: header.0,
        target: header.1,
        map: entries,
    }
}

fn parse_map_entry(line: &str) -> (Range<u64>, Range<u64>) {
    let [target_start, source_start, count] = line
        .split_whitespace()
        .map(|item| item.parse::<u64>().unwrap())
        .collect::<Vec<u64>>()[..3]
    else {
        todo!()
    };

    let source_range = source_start..(source_start + count);
    let target_range = target_start..(target_start + count);

    return (source_range, target_range);
}

fn parse_header(line: &str) -> (String, String) {
    let text: Vec<&str> = line.split_whitespace().nth(0).unwrap().split('-').collect();
    return (
        text.first().unwrap().to_string(),
        text.last().unwrap().to_string(),
    );
}

fn parse_seed(line: &str) -> Vec<Range<u64>> {
    let values: Vec<u64> = line
        .split(":")
        .nth(1)
        .unwrap()
        .split_whitespace()
        .map(|x| x.parse::<u64>().unwrap())
        .collect();

    (0..values.len())
        .step_by(2)
        .map(|i| values[i]..values[i] + values[i + 1])
        .collect()
}

fn find_matching(
    maps: &Vec<Map>,
    to_find: Vec<Range<u64>>,
    source: &String,
    stop_at: String,
) -> Vec<Range<u64>> {
    if source.eq(&stop_at) {
        return to_find;
    }

    let map = maps.iter().find(|x| x.source.eq(source));

    if map.is_none() {
        println!("No map found for source {}", source);
        return to_find;
    }

    let map = map.unwrap();

    let matches = to_find
        .iter()
        .flat_map(|range_to_find| {
            let ranges: Vec<Range<u64>> = map
                .map
                .iter()
                .filter_map(|tuples| {
                    if range_to_find.start == tuples.0.start && range_to_find.end == tuples.0.end {
                        return Some(tuples.1.clone());
                    }
                    //   +-----------------+
                    // +-----------------------+
                    else if tuples.0.start < range_to_find.start && range_to_find.end < tuples.0.end {
                        let a = range_to_find.start - tuples.0.start;
                        let b = tuples.0.end - range_to_find.end;
                        return Some((tuples.1.start + a)..(tuples.1.end - b));
                    }
                    //  +---------------------------+
                    //     +---------------------+
                    else if range_to_find.start <= tuples.0.start
                        && range_to_find.end >= tuples.0.end
                        && range_to_find.start < tuples.0.end
                        && range_to_find.end > tuples.0.start
                    {
                        return Some(tuples.1.clone());
                    }
                    //    +----------------------+
                    //  +--------------------+
                    else if range_to_find.start >= tuples.0.start
                        && range_to_find.end >= tuples.0.end
                        && range_to_find.start < tuples.0.end
                        && range_to_find.end > tuples.0.start
                    {
                        return Some((tuples.1.start + (range_to_find.start - tuples.0.start))..tuples.1.end);
                    }
                    //  +----------------------+
                    //    +----------------------+
                    else if range_to_find.start <= tuples.0.start
                        && range_to_find.end <= tuples.0.end
                        && range_to_find.end > tuples.0.start
                        && range_to_find.start < tuples.0.end
                    {
                        return Some(tuples.1.start..(tuples.1.end - (tuples.0.end - range_to_find.end)));
                    }
                    None
                })
                .collect();
            return ranges;
        })
        .collect();

    find_matching(maps, matches, &map.target, stop_at)
}
