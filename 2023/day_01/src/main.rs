use std::{
    fmt::format,
    fs::File,
    io::{BufRead, BufReader},
    ops::Index,
};

fn get_digits<'a>() -> Vec<(&'a str, i32)> {
    vec![
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
    ("0", 0),
    ("1", 1),
    ("2", 2),
    ("3", 3),
    ("4", 4),
    ("5", 5),
    ("6", 6),
    ("7", 7),
    ("8", 8),
    ("9", 9),
    ]
}
fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut values: Vec<i32> = Vec::new();

    for (_, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let first = get_first_digit(&line);
        let last = get_last_digit(&line);

        let value = first *10 + last;
        values.push(value);
    }

    println!("{}", values.len());

    println!("{}", values.into_iter().sum::<i32>())
}

fn get_last_digit(string: &str) -> i32 {
    let start = get_digits()
        .iter()
        .map(|item| match string.rfind(item.0) {
            Some(val) => Some((val, item.1)),
            None => None,
        })
        .filter(Option::is_some)
        .max_by_key(|a| a.unwrap().0)
        .unwrap()
        .unwrap();

    start.1
}
fn get_first_digit(string: &str) -> i32 {
    let start = get_digits()
        .iter()
        .map(|item| match string.find(item.0) {
            Some(val) => Some((val, item.1)),
            None => None,
        })
        .filter(Option::is_some)
        .min_by_key(|a| a.unwrap().0)
        .unwrap()
        .unwrap();

    start.1
}


pub fn verse(n: u32) -> String {
    println!("{}", n);
    format!("{} bottles of beer on the wall, {} bottles of beer.
Take one down and pass it around, {} bottles of beer on the wall.
", n, n, n-1)
}

pub fn sing(start: u32, end: u32) -> String {
    (start..end)
    .into_iter()
    .map(|value|verse(value))
    .collect::<Vec<String>>()
    .join("\n")
}