use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let pipes: Vec<Vec<Pipe>> = reader
        .lines()
        .map(|s| s.unwrap().chars().map(|c| Pipe::from(c)).collect())
        .collect();

    let start_index = pipes
        .iter()
        .enumerate()
        .find_map(|(index, line)| {
            line.iter()
                .enumerate()
                .find(|(_, pipe)| match pipe {
                    Pipe::START => true,
                    _ => false,
                })
                .map(|(i, _)| (i, index))
        })
        .unwrap();

    let mut coordinates: Vec<(isize, isize)> = Vec::new();

    let mut x = start_index.0;
    let mut y = start_index.1;

    x -= 1;

    let mut next_pipe = &pipes[y][x];
    let mut from = Cardinal::E;
    if !next_pipe.is_right_connecting() {
        x += 2;
        next_pipe = &pipes[y][x];
        from = Cardinal::W;
        if !next_pipe.is_left_connecting() {
            x -= 1;
            y += 1;
            next_pipe = &pipes[y][x];
            from = Cardinal::N;
            if !next_pipe.is_top_connecting() {
                y -= 2;
                next_pipe = &pipes[y][x];
                from = Cardinal::S;
            }
        }
    }
    coordinates.push((x as isize, y as isize));

    while !next_pipe.eq(&Pipe::START) {
        let next = next_pipe.next_from(&from);

        match next {
            Cardinal::N => y -= 1,
            Cardinal::S => y += 1,
            Cardinal::E => x += 1,
            Cardinal::W => x -= 1,
        }
        from = next.inverse();
        next_pipe = &pipes[y][x];
        coordinates.push((x as isize, y as isize));
        
    }

    let area = (0..(coordinates.len())).map(|i| ((pipes.len() as isize - coordinates[i].1) + (pipes.len() as isize - coordinates[(i +1) % coordinates.len()].1)) * (coordinates[i].0 - coordinates[(i +1) % coordinates.len()].0)).reduce(|acc, x| acc + x).unwrap() as f64 * 0.5;
    let inside_points = area.abs() - (coordinates.len() as f64 / 2.0) + 1.0;
    println!("{}", inside_points);
}

#[derive(PartialEq, Eq)]
enum Pipe {
    NS,
    WE,
    NW,
    NE,
    SW,
    SE,
    START,
    NONE,
}

enum Cardinal {
    N,
    S,
    E,
    W,
}

impl Pipe {
    fn is_right_connecting(&self) -> bool {
        match self {
            Self::WE | Self::NE | Self::SE => true,
            _ => false,
        }
    }

    fn is_left_connecting(&self) -> bool {
        match self {
            Self::WE | Self::NW | Self::SW => true,
            _ => false,
        }
    }

    fn is_top_connecting(&self) -> bool {
        match self {
            Self::NS | Self::NW | Self::NE => true,
            _ => false,
        }
    }

    fn is_bottom_connecting(&self) -> bool {
        match self {
            Self::NS | Self::SW | Self::SE => true,
            _ => false,
        }
    }

    fn next_from(&self, direction: &Cardinal) -> Cardinal {
        match (self, direction) {
            (Pipe::NS, Cardinal::N) => Cardinal::S,
            (Pipe::NS, Cardinal::S) => Cardinal::N,
            (Pipe::WE, Cardinal::E) => Cardinal::W,
            (Pipe::WE, Cardinal::W) => Cardinal::E,
            (Pipe::NW, Cardinal::N) => Cardinal::W,
            (Pipe::NW, Cardinal::W) => Cardinal::N,
            (Pipe::NE, Cardinal::N) => Cardinal::E,
            (Pipe::NE, Cardinal::E) => Cardinal::N,
            (Pipe::SW, Cardinal::S) => Cardinal::W,
            (Pipe::SW, Cardinal::W) => Cardinal::S,
            (Pipe::SE, Cardinal::S) => Cardinal::E,
            (Pipe::SE, Cardinal::E) => Cardinal::S,
            _ => Cardinal::N,
        }
    }
}

impl From<char> for Pipe {
    fn from(value: char) -> Self {
        match value {
            '-' => Pipe::WE,
            '|' => Pipe::NS,
            'L' => Pipe::NE,
            'J' => Pipe::NW,
            '7' => Pipe::SW,
            'F' => Pipe::SE,
            'S' => Pipe::START,
            _ => Pipe::NONE,
        }
    }
}

impl Cardinal {
    fn inverse(&self) -> Cardinal {
        match self {
            Cardinal::N => Cardinal::S,
            Cardinal::S => Cardinal::N,
            Cardinal::E => Cardinal::W,
            Cardinal::W => Cardinal::E,
        }
    }
}
