use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
    ops::{Add, AddAssign},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().map(|line| line.unwrap()).collect();
    let mut map = SpaceMap::from(lines);
    map.expand();
    let sum = sum_shortest_paths(&map);

    println!("{}", sum);
}

struct Galaxy {
    x: u64,
    y: u64,
}

struct SpaceMap {
    galaxies: Vec<Galaxy>,
}

impl SpaceMap {
    fn expand(&mut self) {
        //Expand Row
        let mut y = 0;
        let mut max_y = self
            .galaxies
            .iter()
            .max_by_key(|galaxy| galaxy.y)
            .unwrap()
            .y
            .clone();

        while y <= max_y {
            let has_galaxy = self.galaxies.iter().any(|galaxy| galaxy.y == y as u64);
            if !has_galaxy {
                self.galaxies
                    .iter_mut()
                    .filter(|galaxy| galaxy.y > y as u64)
                    .for_each(|galaxy| galaxy.y += 1_000_000 - 1);
                max_y += 1_000_000 - 1;
                y += 1_000_000;
            } else {
                y += 1;
            }
        }

        //Expand columns
        let mut x = 0;
        let mut max_x = self
            .galaxies
            .iter()
            .max_by_key(|galaxy| galaxy.x)
            .unwrap()
            .x
            .clone();
        while x <= max_x {
            let has_galaxy = self.galaxies.iter().any(|galaxy| galaxy.x == x as u64);
            if !has_galaxy {
                self.galaxies
                    .iter_mut()
                    .filter(|galaxy| galaxy.x > x as u64)
                    .for_each(|galaxy| galaxy.x += 1_000_000 - 1);
                max_x += 1_000_000 - 1;
                x += 1_000_000;
            } else {
                x += 1;
            }
        }
    }

    fn shortest_path_length_between(&self, a: usize, b: usize) -> u64 {
        let first = &self.galaxies[a];
        let second = &self.galaxies[b];

        first.x.abs_diff(second.x) + first.y.abs_diff(second.y)
    }
}

impl From<Vec<String>> for SpaceMap {
    fn from(lines: Vec<String>) -> Self {
        let map = lines
            .iter()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars().enumerate().filter_map(move |(x, c)| {
                    if c == '#' {
                        let out = Galaxy {
                            x: x as u64,
                            y: y as u64,
                        };
                        return Some(out);
                    } else {
                        return None;
                    }
                })
            })
            .collect();
        Self { galaxies: map }
    }
}

fn sum_shortest_paths(map: &SpaceMap) -> u64 {
    (0..map.galaxies.len())
        .flat_map(|left| {
            ((left + 1)..map.galaxies.len())
                .map(move |right| map.shortest_path_length_between(left, right))
        })
        .sum()
}
