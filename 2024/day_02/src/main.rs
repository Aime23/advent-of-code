use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut reports: Vec<Report> = Vec::new();

    for line in reader.lines() {
        let line = line.unwrap();
        reports.push(Report::from(line));
    }

    let out = reports.iter().filter(|x| x.is_valid()).count();

    println!("{}", out);
}

struct Report {
    levels: Vec<i32>,
}
impl From<String> for Report {
    fn from(value: String) -> Self {
        return Report {
            levels: value
                .split_whitespace()
                .map(|x| x.parse().unwrap())
                .collect(),
        };
    }
}

impl Report {
    fn is_valid(&self) -> bool {
        let increasing = self.levels[0] < self.levels[1];
        for level_index in 0..self.levels.len() - 1 {
            if (!increasing && self.levels[level_index] < self.levels[level_index + 1])
                || (increasing && self.levels[level_index] > self.levels[level_index + 1])
            {
                return false;
            }
            let delta = (self.levels[level_index] - self.levels[level_index + 1]).abs();
            if delta < 1 || delta > 3 {
                return false;
            }
        }
        return true;
    }
}
