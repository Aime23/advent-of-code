use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file input.txt");
    let reader = BufReader::new(file);

    let mut first_list: Vec<i64> = Vec::new();
    let mut second_list: Vec<i64> = Vec::new();

    for line in reader.lines() {
        let line = line.unwrap();
        let mut pair = line.split_whitespace().map(|x| x.parse::<i64>().unwrap());
        first_list.push(pair.next().unwrap());
        second_list.push(pair.next().unwrap());
    }

    let mut first_map: HashMap<i64, i64> = HashMap::new();
    let mut second_map: HashMap<i64, i64> = HashMap::new();
    for (first, second) in first_list.into_iter().zip(second_list.into_iter()) {
        first_map.insert(first, first_map.get(&first).unwrap_or(&0) + 1);
        second_map.insert(second, second_map.get(&second).unwrap_or(&0) + 1);
    }
    let out: i64 = first_map
        .keys()
        .map(|x| x * first_map.get(x).unwrap_or(&0) * second_map.get(x).unwrap_or(&0))
        .sum();

    println!("{}", out);
}
